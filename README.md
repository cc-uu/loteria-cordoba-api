# API de Juegos - Lotería de Córdoba

Este repositorio contiene el código fuente de la **API de Juegos** de la [Lotería de Córdoba](https://loteriadecordoba.com.ar/). La API está desarrollada utilizando el framework Yii2 y tiene como objetivo principal manejar las peticiones a la base de datos de la Lotería de Córdoba, sirviendo exclusivamente datos de dicha base de datos.

## Información General

- **URL Base de la API**: [https://api.loteriadecordoba.com.ar/api/web/v1/](https://api.loteriadecordoba.com.ar/api/web/v1/)
- **Framework**: Yii2
- **Versión de PHP en Producción**: 7.0.27
- **Servidor Web**: Apache HTTP Server
- **Gestión de Dependencias**: Composer

## Estructura del Proyecto

El proyecto está organizado en las siguientes carpetas y archivos principales:

- `api/config/`: Contiene archivos de configuración de la API.
- `api/modules/v1/controllers/`: Controladores que manejan las peticiones HTTP.
- `api/modules/v1/models/`: Modelos que representan la estructura de los datos y las reglas de negocio.
- `api/runtime/`: Archivos generados en tiempo de ejecución.
- `vendor/`: Dependencias gestionadas por Composer.


## Requisitos

- PHP 7.0.27 o superior
- Composer
- Servidor web Apache

## Instalación

1. Clona el repositorio en tu máquina local:
   ```sh
   git clone https://github.com/usuario/api-de-juegos.git
   ```
2. Navega al directorio del proyecto:
   ```sh
   cd api-de-juegos
   ```
3. Instala las dependencias utilizando Composer:
   ```sh
   composer install
   ```

## Configuración

Asegúrate de configurar correctamente los siguientes archivos:

- `api/config/`: Contiene archivos de configuración de la API.


## Uso de la API

La API expone varios endpoints para interactuar con la base de datos de la Lotería de Córdoba.
