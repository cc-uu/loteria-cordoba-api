<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

// ID_DESCRIPCION 66 NO TRAER
// 06813 NO ES 6813 USAR EL 0

class TengoPremioController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\TengoPremio';

    public function behaviors(){
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionSorteosQuiniela(){
		$connection = Yii::$app->db;
		$mes=date('m');
        $anio=date('Y');
        if ($mes==1 || $mes==2 ){
            $fechaIn="01/".date('m')."/".$anio;
        }else{
            $fechaIn="01/".(date('m')-1)."/".$anio;
        }

		/*$xQuery8="SELECT SORTEO, TO_CHAR(FECHA_PRESCRIP, 'DD/MM/YYYY') AS FECHA FROM LECTURA_TICKET.PARAMETRO_LECTURA WHERE FECHA_PRESCRIP >= TO_DATE('".$fechaIn."','DD/MM/YYYY') AND COD_JUEGO='8' ORDER BY SORTEO DESC";
		$xQuery2="SELECT SORTEO, TO_CHAR(FECHA_PRESCRIP, 'DD/MM/YYYY') AS FECHA FROM LECTURA_TICKET.PARAMETRO_LECTURA WHERE FECHA_PRESCRIP >= TO_DATE('".$fechaIn."','DD/MM/YYYY') AND COD_JUEGO='2' ORDER BY SORTEO DESC";
		$xQuery3="SELECT SORTEO, TO_CHAR(FECHA_PRESCRIP, 'DD/MM/YYYY') AS FECHA FROM LECTURA_TICKET.PARAMETRO_LECTURA WHERE FECHA_PRESCRIP >= TO_DATE('".$fechaIn."','DD/MM/YYYY') AND COD_JUEGO='3' ORDER BY SORTEO DESC";
		$xQuery24="SELECT SORTEO, TO_CHAR(FECHA_PRESCRIP, 'DD/MM/YYYY') AS FECHA FROM LECTURA_TICKET.PARAMETRO_LECTURA WHERE FECHA_PRESCRIP >= TO_DATE('".$fechaIn."','DD/MM/YYYY') AND COD_JUEGO='24' ORDER BY SORTEO DESC";
		$xQuery26="SELECT SORTEO, TO_CHAR(FECHA_PRESCRIP, 'DD/MM/YYYY') AS FECHA FROM LECTURA_TICKET.PARAMETRO_LECTURA WHERE FECHA_PRESCRIP >= TO_DATE('".$fechaIn."','DD/MM/YYYY') AND COD_JUEGO='26' ORDER BY SORTEO DESC";
		$s8=$connection->createCommand($xQuery8)->queryAll();
		$s2=$connection->createCommand($xQuery2)->queryAll();
		$s3=$connection->createCommand($xQuery3)->queryAll();
		$s24=$connection->createCommand($xQuery24)->queryAll();
		$s26=$connection->createCommand($xQuery26)->queryAll();*/

        /*$xParametros=array(
        'sorteo26'=>$s26,
        'sorteo24'=>$s24,
        'sorteo8'=>$s8,
        'sorteo2'=>$s2,
        'sorteo3'=>$s3);*/
        $request = Yii::$app->request;
        $billete= "5005145100003312";
        $xQuery1= "select sum(nvl(p.importe,0)) premio from KANBAN.t_premios p where p.ocr = '".$billete."'";
        $xPremios = $connection->createCommand($xQuery1)->queryAll();
        // print_r($xPremios);
        $xQuery="SELECT TO_CHAR(FECHA,'DD/MM/YYYY') AS FECHA1, SORTEO, MODALIDAD FROM PARQUIN WHERE TO_CHAR(FECHA,'yyyy')>='".$anio."' ORDER BY FECHA DESC";
        $xSorteos = $connection->createCommand($xQuery)->queryAll();

        $xParametros=array('sorteos'=>$xSorteos, 'premios', $xPremios);

		return $xParametros;
	}

  // public function actionBuscarPremio(){
  //   $request = Yii::$app->request;
  //   $billete= "5005145100003312";
  //   $xQuery1= "select sum(nvl(p.importe,0)) premio from KANBAN.t_premios p where p.ocr = '".$billete."'";
  //   $xPremios = $connection->createCommand($xQuery1)->queryAll();
  //   $xParametros=array('premios', $xPremios);
  //   var_dump($xParametros);
  //   return $xParametros;
  // }


	public function actionQuinielaBuscar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$sorteo=$request->get('sorteo');
		$billete=$request->get('billete');
		$xQuery="SELECT * FROM KANBAN.T_PREMIOS WHERE SORTEO='".$sorteo."' AND OCR='".$billete."'";
		$xResultado=$connection->createCommand($xQuery)->queryAll();
		return $xResultado;
	}

	public function actionQuinielaCierre(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$suc_ban=$request->get('suc_ban');
		$agencia=$request->get('agencia');
		$fecha=date('d-m-y');
		$xQuery="SELECT * FROM JUEGOS.cierre WHERE nro_agen='".$agencia."' AND suc_ban='".$suc_ban."' AND FEC_DES <= to_date('".$fecha."','dd/mm/yy') AND FEC_HAS >= to_date('".$fecha."','dd/mm/yy') ORDER BY nro_agen";
		$xResultado=$connection->createCommand($xQuery)->queryAll();
		return $xResultado;
	}

	public function actionSorteosLoteria(){
		$connection = Yii::$app->db;
		$xQuery="SELECT TO_CHAR(FECHA,'DD/MM/YYYY') AS FECHA, SORTEO FROM PARLOTE ORDER BY SORTEO DESC";
		$xResultado=$connection->createCommand($xQuery)->queryAll();
		return $xResultado;
	}

	public function actionLoteriaBuscar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$sorteo=$request->get('sorteo');
		$billete=$request->get('billete');
    $xQuery= "select sum(nvl(p.importe,0)) premio from KANBAN.t_premios p where p.ocr = '".$billete."'";
		// $xQuery='SELECT SUM(IMPORTE) AS PREMIO FROM (SELECT CONCEPTO,IMPORTE FROM KANBAN.T_PREMIOS WHERE SORTEO= '.$sorteo.' AND ID_JUEGO=1 AND ID_DESCRIPCION <> 66 AND BILLETE = '.$billete.' GROUP BY CONCEPTO,IMPORTE)';
		$xResultado=$connection->createCommand($xQuery)->queryAll();
		return $xResultado;
	}

	public function actionSorteosTotoBingo(){
		$connection = Yii::$app->db;
		$xQuery="SELECT distinct SORTEO, TO_CHAR(FECHA_SORTEO,'dd/mm/yyyy') AS FECHA_SORTEO FROM kanban.T_TT_EXTRACTO ORDER BY SORTEO DESC";
		$xResultado=$connection->createCommand($xQuery)->queryAll();
		return $xResultado;
	}

	public function actionTotoBingoBuscar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$sorteo=$request->get('sorteo');
		$billete=$request->get('billete');
		$xQuery="SELECT * FROM kanban.T_PREMIOS WHERE SORTEO='".$sorteo."' AND BILLETE='".$billete."' AND ID_DESCRIPCION != 66";
		$xResultado=$connection->createCommand($xQuery)->queryAll();
		return $xResultado;
	}

	public function actionSorteosTelebingoCordobes(){
		$connection = Yii::$app->db;
		$mes=date('m');
        $anio=date('Y');
        if ($mes=1 || $mes=2){
            $fecha="8/".($anio-1);
        }else{
            $fecha=(date('m')-2)."/".date('Y');
        }
		$xQuery="SELECT TO_CHAR(FECHA,'dd/mm/yyyy') AS FECHA, SORTEO FROM extractotelebingo WHERE FECHA >= to_date('".$fecha."','mm/yyyy') GROUP BY FECHA,SORTEO ORDER BY SORTEO DESC";
		$xResultado=$connection->createCommand($xQuery)->queryAll();
		return $xResultado;
	}

	public function actionTelebingoCordobesBuscar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$sorteo=$request->get('sorteo');
		$billete=$request->get('billete');
		$xQuery="SELECT * FROM premiostelebingo WHERE sorteo='".$sorteo."' AND billete='".$billete."'";
		$xResultado=$connection->createCommand($xQuery)->queryAll();
		return $xResultado;
	}

	public function actionSorteosRebingo(){
		$connection = Yii::$app->db;
		$mes=date('m');
        $anio=date('Y');
        if ($mes=1 || $mes=2){
            $fecha="8/".($anio-1);
        }else{
            $fecha=(date('m')-2)."/".date('Y');
        }
		$xQuery="SELECT TO_CHAR(FECHA,'dd/mm/yyyy') AS FECHA, SORTEO FROM extractorebingo WHERE FECHA >= to_date('".$fecha."','mm/yyyy') GROUP BY FECHA,SORTEO ORDER BY SORTEO DESC";
		$xResultado=$connection->createCommand($xQuery)->queryAll();
		return $xResultado;
	}

	public function actionRebingoBuscar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$sorteo=$request->get('sorteo');
		$billete=$request->get('billete');
		$xQuery="SELECT * FROM premiosrebingo WHERE sorteo='".$sorteo."' AND billete='".$billete."'";
		$xResultado=$connection->createCommand($xQuery)->queryAll();
		return $xResultado;
	}
}
