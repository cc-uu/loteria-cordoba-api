<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class SalasGpsController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\SalasGps';

    public function behaviors(){
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}
	protected function verbs(){
	    return [
	        'create' => ['POST'],
	        'update' => ['POST'],
	    ];
	}
	public function actionListar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$sal_uid=$request->get('sal_uid');
		$xQuery="SELECT SAL_UID, LATITUD, LONGITUD FROM SALA_GPS";
		if($sal_uid!=""){
			$xQuery="SELECT SAL_UID, LATITUD, LONGITUD FROM SALA_GPS WHERE SAL_UID=".$sal_uid; 
		}		
		$xCoordenadas=$connection->createCommand($xQuery)->queryAll();
		return $xCoordenadas;
	}
	public function actionCreate(){
		$params=$_REQUEST;
	    $model=new SlotsGps();
    	$model->attributes=$params;
 	    $model->save();
	}
	public function actionUpdate($id){
	    $params=$_REQUEST;
	    $model=$this->findModel($id);
	    $model->attributes=$params;
	    $model->save();
	}
}