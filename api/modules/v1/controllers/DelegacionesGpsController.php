<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class DelegacionesGpsController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\DelegacionesGps';

    public function behaviors() 
    {
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}
	protected function verbs(){
	    return [
	        'create' => ['POST'],
	        'update' => ['POST'],
	    ];
	}
	public function actionListar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$del_uid=$request->get('del_uid');
		$xQuery="SELECT DEL_UID, LATITUD, LONGITUD FROM DELEGACION_GPS";
		if($del_uid!=""){
			$xQuery="SELECT DEL_UID, LATITUD, LONGITUD FROM DELEGACION_GPS WHERE DEL_UID=".$del_uid; 
		}		
		$xCoordenadas=$connection->createCommand($xQuery)->queryAll();
		return $xCoordenadas;
	}
	public function actionCreate(){
		$params=$_REQUEST;
	    $model = new DelegacionesGps();
    	$model->attributes=$params;
 	    $model->save();
	}
	public function actionUpdate($id){
	    $params=$_REQUEST;
	    $model = $this->findModel($id);
	    $model->attributes=$params;
	    $model->save();
	}
}