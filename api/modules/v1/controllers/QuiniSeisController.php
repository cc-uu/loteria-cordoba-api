<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class QuiniSeisController extends ActiveController
{
	public $modelClass = 'api\modules\v1\models\QuiniSeis';

    public function behaviors(){
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionSorteos(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$get_sorteo = $request->get('sorteo');
		$mes=date('m');
        $anio=date('Y');
        if ($mes=1 || $mes=2){
            $fecha="8/".($anio-1);
        }else{
            $fecha=(date('m')-2)."/".date('Y');
        }

		$xQuery="SELECT TO_CHAR(FECHA_SORTEO,'dd/mm/yyyy') AS FECHA_SORTEO, SORTEO FROM KANBAN.T_SORTEO WHERE FECHA_SORTEO >= to_date('".$fecha."','mm/yyyy') AND ID_JUEGO = 4 ORDER BY SORTEO DESC";

		if($get_sorteo=="max")
		{
			$xQuery="SELECT TO_CHAR(FECHA_SORTEO,'dd/mm/yyyy') AS FECHA_SORTEO, SORTEO FROM KANBAN.T_SORTEO WHERE ID_JUEGO = 4 AND ROWNUM = 1
ORDER BY SORTEO DESC";
		}
		else if($get_sorteo!="" && $get_sorteo!="max")
		{
			$xQuery="SELECT TO_CHAR(FECHA_SORTEO,'dd/mm/yyyy') AS FECHA_SORTEO, SORTEO FROM KANBAN.T_SORTEO WHERE FECHA_SORTEO >= to_date('".$fecha."','mm/yyyy') AND SORTEO='".$get_sorteo."' AND ID_JUEGO = 4 GROUP BY FECHA_SORTEO,SORTEO ORDER BY SORTEO DESC";
		}

		$xSorteos=$connection->createCommand($xQuery)->queryAll();
		return $xSorteos;
	}

	public function actionExtracto(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$get_sorteo = $request->get('sorteo');
		$xQueryRondaUno="SELECT * FROM web01.extractoqui6 WHERE sorteo='".$get_sorteo."' ORDER BY bolilla";
		$xExtractoRondaUno=$connection->createCommand($xQueryRondaUno)->queryAll();
		return $xExtractoRondaUno;
	}
}
