<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class TotoBingoController extends ActiveController
{
	public $modelClass = 'api\modules\v1\models\TotoBingo';

    public function behaviors(){
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionSorteos(){
		$connection = Yii::$app->db;	
		$xQuery="SELECT distinct SORTEO, TO_CHAR(FECHA_SORTEO,'dd/mm/yyyy') AS FECHA_SORTEO FROM kanban.T_TT_EXTRACTO ORDER BY SORTEO DESC";
		$xSorteos=$connection->createCommand($xQuery)->queryAll();
		return $xSorteos;
	}

	public function actionExtracto(){
		$connection = Yii::$app->db;	
		$request = Yii::$app->request;
		$sorteo = $request->get('sorteo');
		$xQuery="SELECT TO_CHAR(FECHA_SORTEO,'dd/mm/yyyy')AS FECHA_SORTEO,EXTRAC_ESTRELLA,SORTEO,EXTRAC_SIEMPRE_SALE,EXTRAC_POZO_MILL FROM kanban.T_TT_EXTRACTO WHERE SORTEO IN (SELECT max(SORTEO) from kanban.T_TT_EXTRACTO)";
		if ($sorteo!=""){
			$xQuery="SELECT TO_CHAR(FECHA_SORTEO,'dd/mm/yyyy')AS FECHA_SORTEO,EXTRAC_ESTRELLA,SORTEO,EXTRAC_SIEMPRE_SALE,EXTRAC_POZO_MILL FROM kanban.T_TT_EXTRACTO WHERE SORTEO =".$sorteo;
		}
		$xExtracto=$connection->createCommand($xQuery)->queryAll();
		return $xExtracto;
	}
}