<?php

namespace api\modules\v1\controllers;

use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class DondeEstaMiBilleteController extends ActiveController
{
	public $modelClass = 'api\modules\v1\models\DondeEstaMiBillete';

	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => QueryParamAuth::className(),
		];
		return $behaviors;
	}

	public function actionSorteos()
	{
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$sorteo = $request->get('sorteo');
		$billete = $request->get('billete');
		$xQuery = "SELECT SORTEO, TO_CHAR(FECHA_SORTEO, 'DD/MM/YYYY') AS FECHA FROM KANBAN.T_SORTEO WHERE ID_JUEGO=1 AND FECHA_BAJA IS NULL AND FECHA_SORTEO > SYSDATE ORDER BY SORTEO DESC";
		$xSorteo = $connection->createCommand($xQuery)->queryAll();

		return $xSorteo;
	}

	public function actionSorteo() 
	{

		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$sorteo = $request->get('sorteo');
		$billete = $request->get('billete');
		$xQuery = "SELECT x.fracciones,
		(CASE
				WHEN x.suc_ban_reserva != 36
						THEN
						(SELECT ('Suc.: '|| js.Nombre ||' - Agencia:' || x.nro_agen_reserva)
						FROM JUEGOS.sucursal js
						WHERE  js.suc_ban=x.suc_ban_reserva )
				ELSE 'En cualquier agencia'
		END) AS comprar_en
		FROM (
				SELECT (COUNT(Sb.Billete) - (SELECT COUNT(ri.Billete)
								FROM Kanban.T_Reparto_Inteligente Ri
								WHERE Ri.Id_Juego = sb.Id_Juego
										AND Ri.Sorteo = sb.Sorteo
										AND Ri.Billete = sb.Billete
						)) AS fracciones, sb.suc_ban_reserva, sb.nro_agen_reserva
				FROM Kanban.T_Reparto_Sorteo_Boldt sb
				WHERE sb.Id_Juego = 1
						AND sb.Sorteo = $sorteo
						AND sb.Billete = $billete
				GROUP BY sb.Id_Juego, sb.Sorteo, sb.Billete, sb.suc_ban_Reserva, sb.nro_agen_reserva
		) x";

		$xResultado = $connection->createCommand($xQuery)->queryAll();
		return $xResultado;
	}
}
