<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class BarriosController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Barrios';

    public function behaviors() 
    {
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionListar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$localidad=$request->get('localidad');
		$xQuery = "SELECT barrio FROM juegos.agencia WHERE barrio IS NOT NULL AND nro_agen <= 3007 AND localidad LIKE '%".$localidad."%' GROUP BY barrio ORDER BY barrio";
		$xBarrios=$connection->createCommand($xQuery)->queryAll();
		$xParametros=array('barrios'=>$xBarrios);
		return $xParametros;
	}
}