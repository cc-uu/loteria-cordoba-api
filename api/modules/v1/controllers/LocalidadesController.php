<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class LocalidadesController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Localidades';

    public function behaviors() 
    {
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionListar(){
		$connection = Yii::$app->db;
		$xQuery = "SELECT localidad FROM juegos.agencia WHERE localidad IS NOT NULL AND nro_agen <= 3007 GROUP BY localidad ORDER BY localidad";
		$xLocalidades=$connection->createCommand($xQuery)->queryAll();
		$xParametros=array('localidades'=>$xLocalidades);
		return $xParametros;
	}
}