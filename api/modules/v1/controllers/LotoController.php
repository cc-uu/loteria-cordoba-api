<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class LotoController extends ActiveController
{
	public $modelClass = 'api\modules\v1\models\Loto';

    public function behaviors(){
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionPozoEstimado(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$get_sorteo = $request->get('sorteo');
		if ($get_sorteo=="max"){
			$xQuery="SELECT TO_CHAR(FECHA,'dd/mm/yyyy')AS FECHA,POZO_ESTIMADO,SORTEO FROM parlot WHERE sorteo IN (SELECT MAX(sorteo) FROM parlot )";
		}else if ($get_sorteo!="max"){
			$xQuery="SELECT TO_CHAR(FECHA,'dd/mm/yyyy')AS FECHA,POZO_ESTIMADO,SORTEO FROM parlot WHERE sorteo='".$get_sorteo."'";
		}
		$xPozo=$connection->createCommand($xQuery)->queryAll();
		return $xPozo;
		
	}

	public function actionExtracto(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$get_sorteo = $request->get('sorteo');
		$xQueryTradicional="SELECT * FROM extractoloto1 WHERE sorteo='".$get_sorteo."' AND MODALIDAD='TRADICIONAL' ORDER BY bolilla";
		$xTradicional=$connection->createCommand($xQueryTradicional)->queryAll();
		$xQueryDesquite="SELECT * FROM extractoloto1 WHERE sorteo='".$get_sorteo."' AND MODALIDAD='DESQUITE' ORDER BY bolilla";
		$xDesquite=$connection->createCommand($xQueryDesquite)->queryAll();
		$xQuerySale="SELECT * FROM extractoloto1 WHERE sorteo='".$get_sorteo."' AND MODALIDAD='SALE O SALE' ORDER BY bolilla";
		$xSale=$connection->createCommand($xQuerySale)->queryAll();
		$xParametros=array(
			'tradicional'=>$xTradicional,
			'desquite'=>$xDesquite,
			'sale'=>$xSale);
		return $xParametros;
	}

	public function actionAciertos(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$get_sorteo = $request->get('sorteo');
		$xQueryTradicional="SELECT * FROM aciertoloto1 WHERE sorteo='".$get_sorteo."' AND MODALIDAD='TRADICIONAL' ORDER BY acierto DESC";
		$xTradicional=$connection->createCommand($xQueryTradicional)->queryAll();
		$xQueryDesquite="SELECT * FROM aciertoloto1 WHERE sorteo='".$get_sorteo."' AND MODALIDAD='DESQUITE' ORDER BY acierto DESC";
		$xDesquite=$connection->createCommand($xQueryDesquite)->queryAll();
		$xQuerySale="SELECT * FROM aciertoloto1 WHERE sorteo='".$get_sorteo."' AND MODALIDAD='SALE O SALE' ORDER BY acierto DESC";
		$xSale=$connection->createCommand($xQuerySale)->queryAll();
		$xParametros=array(
			'tradicional'=>$xTradicional,
			'desquite'=>$xDesquite, 
			'sale'=>$xSale);
		return $xParametros;
	}

	public function actionSorteos(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$get_sorteo = $request->get('sorteo');
		$mes=date('m');
        $anio=date('Y');
        if ($mes=1 || $mes=2){
            $fecha="8/".($anio-1);
        }else{
            $fecha=(date('m')-2)."/".date('Y');
        }	
		$xQuery="SELECT TO_CHAR(FECHA,'dd/mm/yyyy') AS FECHA, SORTEO FROM PARLOT WHERE FECHA >= to_date('".$fecha."','mm/yyyy') ORDER BY SORTEO DESC";
		if($get_sorteo=="max"){
			$xQuery="SELECT TO_CHAR(FECHA,'dd/mm/yyyy') AS FECHA, SORTEO FROM PARLOT WHERE SORTEO IN (SELECT MAX(SORTEO) FROM PARLOT) AND FECHA >= to_date('".$fecha."','mm/yyyy')";
		}else if($get_sorteo!="" && $get_sorteo!="max"){
			$xQuery="SELECT TO_CHAR(FECHA,'dd/mm/yyyy') AS FECHA, SORTEO FROM PARLOT WHERE SORTEO = '".$get_sorteo."' AND FECHA >= to_date('".$fecha."','mm/yyyy')";
		}
		$xSorteos=$connection->createCommand($xQuery)->queryAll();
		return $xSorteos;
	}
}