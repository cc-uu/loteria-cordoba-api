<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseStringHelper;
use yii\db\Query;

class QuinielaController extends ActiveController
{
	public $modelClass = 'api\modules\v1\models\Quiniela';

    public function behaviors(){
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionSorteosNoGanadores(){
		$connection = Yii::$app->db;
		$xQuery="SELECT TO_CHAR(FECHA_SORTEO, 'DD/MM/YYYY') AS FECHA, SORTEO FROM KANBAN.T_SORTEO WHERE ID_JUEGO = 30 AND FECHA_SORTEO <= SYSDATE ORDER BY SORTEO DESC";
		$xSorteo=$connection->createCommand($xQuery)->queryAll();
		return $xSorteo;
	}

	public function actionSorteo(){
		$connection = Yii::$app->db;
		$xQuery="SELECT TO_CHAR(FECHA, 'DD/MM/YYYY') AS FECHA, MODALIDAD, SORTEO, TO_CHAR(FECHA, 'DY') AS FECHA_D FROM PARQUIN
		WHERE FECHA IN(SELECT MAX(FECHA) FROM PARQUIN) ORDER BY SORTEO ASC";
		$xSorteo=$connection->createCommand($xQuery)->queryAll();
		return $xSorteo;
	}

	public function actionFecha(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$xQuery="SELECT TO_CHAR(FECHA, 'DD/MM/YYYY') AS FECHA FROM PARQUIN WHERE SORTEO=".$request->get('sorteo');
		$xFecha=$connection->createCommand($xQuery)->queryAll();
		return $xFecha;
	}

	public function actionBuscar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$xSorteo = $request->get('sorteo');
		list($sorteo, $modalidad) = explode('-', $xSorteo);
		$xQuery = "SELECT E.BOLILLA, E.LOTERIA, E.POSICION, E.ORDEN, E.BOLILLA2, E.SORTEO, P.MODALIDAD FROM EXTRACTOQUIN E, PARQUIN P WHERE E.SORTEO = ".$sorteo."
		AND P.SORTEO = ".$sorteo."
		AND P.MODALIDAD='".$modalidad."'
		ORDER BY E.POSICION ASC";
		$xExtracto = $connection->createCommand($xQuery)->queryAll();
		return $xExtracto;
	}

	# TRAER DE KANBAN.T_PREMIOS ID_JUEGO 2, OCR = NRO BILLETE Y NRO DE SORTEO;
	# REVISAR COMBO SORTEOS EN EL TENGO PREMIO GENERAL

	public function actionSorteos(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$nro_sorteo = $request->get('nro');
		$xAnio=date('Y')-1;
		$xQuery="SELECT TO_CHAR(FECHA,'DD/MM/YYYY') AS FECHA1, SORTEO, MODALIDAD FROM PARQUIN WHERE TO_CHAR(FECHA,'yyyy')>='".$xAnio."' ORDER BY FECHA DESC";

		if ($nro_sorteo!=NULL){
			$xQuery="SELECT TO_CHAR(FECHA,'DD/MM/YYYY') AS FECHA1, SORTEO, MODALIDAD FROM PARQUIN WHERE TO_CHAR(FECHA,'yyyy')>='".$xAnio."' AND SORTEO = ".$nro_sorteo." ORDER BY FECHA DESC";
		}

		$sorteos=$connection->createCommand($xQuery)->queryAll();
		return $sorteos;
	}

	public function actionCabeza(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$xSorteo = $request->get('sorteo');
		$xLoteria = $request->get('loteria');
		$bolilla=null;
		$xQuery = "SELECT BOLILLA FROM EXTRACTOQUIN WHERE SORTEO = ".$xSorteo." AND LOTERIA=".$xLoteria." AND POSICION = 1";
		$xExtracto = $connection->createCommand($xQuery)->queryAll();
		return $xExtracto;
	}

	public function actionExtracto(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$xSorteo = $request->get('sorteo');
		$xLoteria = $request->get('loteria');
		$xQuery = "SELECT * FROM EXTRACTOQUIN WHERE SORTEO = ".$xSorteo." AND LOTERIA=".$xLoteria." ORDER BY POSICION ASC";
		$xExtracto = $connection->createCommand($xQuery)->queryAll();
		return $xExtracto;
	}
}
