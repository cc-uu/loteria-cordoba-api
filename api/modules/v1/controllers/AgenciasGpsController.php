<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class AgenciasGpsController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\AgenciasGps';

    public function behaviors(){
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}
	protected function verbs(){
	    return [
	        'create' => ['POST'],
	        'update' => ['POST'],
	    ];
	}
	public function actionListar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$age_uid=$request->get('age_uid');
		$localidad=$request->get('localidad');
		$agencia=$request->get('agencia');
		$barrio=$request->get('barrio');
		$calle=$request->get('calle');
		$xQuery="SELECT A.AGE_UID, A.LATITUD, A.LONGITUD, J.NRO_AGEN, J.CALLE, J.NRO_CALLE, J.BARRIO, 
			J.LOCALIDAD, J.COD_POSTAL, J.TELEFONO FROM AGENCIA_GPS A, JUEGOS.AGENCIA J WHERE J.AGE_UID=A.AGE_UID";
		
		if($age_uid!=""){
			$xQuery="SELECT AGE_UID, LATITUD, LONGITUD FROM AGENCIA_GPS WHERE AGE_UID=".$age_uid; 
		}
		if($localidad!="" && $agencia==""){
			$xQuery="SELECT A.AGE_UID, A.LATITUD, A.LONGITUD, J.NRO_AGEN, J.CALLE, J.NRO_CALLE, J.BARRIO, 
			J.LOCALIDAD, J.COD_POSTAL, J.TELEFONO
			FROM AGENCIA_GPS A, JUEGOS.AGENCIA J
			WHERE J.LOCALIDAD LIKE '%".$localidad."%'
			AND J.AGE_UID=A.AGE_UID"; 
		}	
		if($localidad!="" && $agencia!=""){
			$xQuery="SELECT A.AGE_UID, A.LATITUD, A.LONGITUD, J.NRO_AGEN, J.CALLE, J.NRO_CALLE, J.BARRIO, 
			J.LOCALIDAD, J.COD_POSTAL, J.TELEFONO
			FROM AGENCIA_GPS A, JUEGOS.AGENCIA J
			WHERE J.LOCALIDAD LIKE '%".$localidad."%'
			AND J.NRO_AGEN=".$agencia."
			AND J.AGE_UID=A.AGE_UID"; 
		}	
		if($localidad!="" && $barrio!=""){
			$xQuery="SELECT A.AGE_UID, A.LATITUD, A.LONGITUD, J.NRO_AGEN, J.CALLE, J.NRO_CALLE, J.BARRIO, 
			J.LOCALIDAD, J.COD_POSTAL, J.TELEFONO
			FROM AGENCIA_GPS A, JUEGOS.AGENCIA J
			WHERE J.LOCALIDAD LIKE '%".$localidad."%'
			AND J.BARRIO='".$barrio."'
			AND J.AGE_UID=A.AGE_UID"; 
		}	
		if($localidad!="" && $barrio!="" && $calle!=""){
			$xQuery="SELECT A.AGE_UID, A.LATITUD, A.LONGITUD, J.NRO_AGEN, J.CALLE, J.NRO_CALLE, J.BARRIO, 
			J.LOCALIDAD, J.COD_POSTAL, J.TELEFONO
			FROM AGENCIA_GPS A, JUEGOS.AGENCIA J
			WHERE J.LOCALIDAD LIKE '%".$localidad."%'
			AND J.BARRIO='".$barrio."'
			AND J.CALLE='".$calle."'
			AND J.AGE_UID=A.AGE_UID"; 
		}		
		if($localidad=="" && $agencia!=""){
			$xQuery="SELECT A.AGE_UID, A.LATITUD, A.LONGITUD, J.NRO_AGEN, J.CALLE, J.NRO_CALLE, J.BARRIO, 
			J.LOCALIDAD, J.COD_POSTAL, J.TELEFONO
			FROM AGENCIA_GPS A, JUEGOS.AGENCIA J
			WHERE J.NRO_AGEN=".$agencia."
			AND J.AGE_UID=A.AGE_UID"; 
		}

		$xCoordenadas=$connection->createCommand($xQuery)->queryAll();
		return $xCoordenadas;
	}
	public function actionCreate(){
		$params=$_REQUEST;
	    $model=new AgenciasGps();
    	$model->attributes=$params;
 	    $model->save();
	}
	public function actionUpdate($id){
	    $params=$_REQUEST;
	    $model=$this->findModel($id);
	    $model->attributes=$params;
	    $model->save();
	}
}