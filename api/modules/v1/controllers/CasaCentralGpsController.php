<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class CasaCentralGpsController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\CasaCentralGps';

    public function behaviors() 
    {
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}
	protected function verbs(){
	    return [
	        'create' => ['POST'],
	        'update' => ['POST'],
	    ];
	}
	public function actionListar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$cen_uid=$request->get('cen_uid');
		$xQuery="SELECT CEN_UID, LATITUD, LONGITUD FROM CASACENTRAL_GPS";
		if($cen_uid!=""){
			$xQuery="SELECT CEN_UID, LATITUD, LONGITUD FROM CASACENTRAL_GPS WHERE CEN_UID=".$cen_uid; 
		}		
		$xCoordenadas=$connection->createCommand($xQuery)->queryAll();
		return $xCoordenadas;
	}
	public function actionCreate(){
		$params=$_REQUEST;
	    $model = new CasaCentralGps();
    	$model->attributes=$params;
 	    $model->save();
	}
	public function actionUpdate($id){
	    $params=$_REQUEST;
	    $model = $this->findModel($id);
	    $model->attributes=$params;
	    $model->save();
	}
}