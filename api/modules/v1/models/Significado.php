<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;

class Significado extends ActiveRecord
{
	public static function tableName()
	{
		return 'SIGNIFICADO';
	}

    public static function primaryKey()
    {
        return ['NUMERO'];
    }
}