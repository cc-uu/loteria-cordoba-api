<?php 
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;

class SalasGps extends ActiveRecord
{
    public static function tableName()
    {
        return 'SALA_GPS';
    }
    public static function primaryKey()
    {
        return ['SAL_UID'];
    }
    public function rules()
    {
        return [
            [['SAL_UID', 'LATITUD', 'LONGITUD'], 'required']
        ];
    }
}