<?php 
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;

class AgenciasGps extends ActiveRecord
{
    public static function tableName()
    {
        return 'AGENCIA_GPS';
    }
    public static function primaryKey()
    {
        return ['AGE_UID'];
    }
    public function rules()
    {
        return [
            [['AGE_UID', 'LATITUD', 'LONGITUD'], 'required']
        ];
    }
}