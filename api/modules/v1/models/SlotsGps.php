<?php 
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;

class SlotsGps extends ActiveRecord
{
    public static function tableName()
    {
        return 'SLOT_GPS';
    }
    public static function primaryKey()
    {
        return ['SLO_UID'];
    }
    public function rules()
    {
        return [
            [['SLO_UID', 'LATITUD', 'LONGITUD', 'LUGAR'], 'required']
        ];
    }
}