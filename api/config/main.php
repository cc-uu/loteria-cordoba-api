<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ],
    ],
    'components' => [
        'request' => [
            'parsers' => [
              'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'user' => [
            'identityClass' => 'api\modules\v1\models\User',
            'enableAutoLogin' => false
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                //SORTEOS QUINIELA NO GANADORES
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/quiniela',
                    'pluralize' => false,
                    'extraPatterns' => ['GET sorteos-no-ganadores' => 'quiniela/sorteos-no-ganadores'],
                ],
                //FECHA SORTEO
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/quiniela',
                    'pluralize' => false,
                    'extraPatterns' => ['GET fecha' => 'fecha'],
                ],
                //SLOTS X SALAS
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/slots-x-salas',
                    'pluralize' => false,
                    'extraPatterns' => ['GET slots-x-salas' => 'slots-x-salas'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/slots-x-salas',
                    'pluralize' => false,
                    'extraPatterns' => ['GET slots-x-salas' => 'slots-x-salas/listar'],
                    'tokens' => ['{sxs_uid}'=>'<sxs_uid:\\w+>'],
                ],
                //SALAS GPS
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/salas-gps',
                    'pluralize' => false,
                    'extraPatterns' => ['GET salas-gps' => 'salas-gps'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/salas-gps',
                    'pluralize' => false,
                    'extraPatterns' => ['GET salas-gps' => 'salas-gps/listar'],
                    'tokens' => ['{sal_uid}'=>'<sal_uid:\\w+>'],
                ],
                //SLOTS GPS
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/slots-gps',
                    'pluralize' => false,
                    'extraPatterns' => ['GET slots-gps' => 'slots-gps'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/slots-gps',
                    'pluralize' => false,
                    'extraPatterns' => ['GET slots-gps' => 'slots-gps/listar'],
                    'tokens' => ['{slo_uid}'=>'<slo_uid:\\w+>'],
                ],
                //CASINOS GPS
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/casinos-gps',
                    'pluralize' => false,
                    'extraPatterns' => ['GET casinos-gps' => 'casinos-gps'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/casinos-gps',
                    'pluralize' => false,
                    'extraPatterns' => ['GET casinos-gps' => 'casinos-gps/listar'],
                    'tokens' => ['{cas_uid}'=>'<cas_uid:\\w+>'],
                ],
                //DELEGACIONES GPS
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/delegaciones-gps',
                    'pluralize' => false,
                    'extraPatterns' => ['GET delegaciones-gps' => 'delegaciones-gps'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/delegaciones-gps',
                    'pluralize' => false,
                    'extraPatterns' => ['GET delegaciones-gps' => 'delegaciones-gps/listar'],
                    'tokens' => ['{cen_uid}'=>'<cen_uid:\\w+>'],
                ],
                //CASA CENTRAL GPS
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/casa-central-gps',
                    'pluralize' => false,
                    'extraPatterns' => ['GET casa-central-gps' => 'casa-central-gps'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/casa-central-gps',
                    'pluralize' => false,
                    'extraPatterns' => ['GET casa-central-gps' => 'casa-central-gps/listar'],
                    'tokens' => ['{del_uid}'=>'<del_uid:\\w+>'],
                ],
                //AGENCIAS GPS
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/agencias-gps',
                    'pluralize' => false,
                    'extraPatterns' => ['GET agencias-gps' => 'agencias-gps'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/agencias-gps',
                    'pluralize' => false,
                    'extraPatterns' => ['GET agencias-gps' => 'agencias-gps/listar'],
                    'tokens' => ['{age_uid}'=>'<age_uid:\\w+>'],
                ],
                //SORTEOS LOTO
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/loto',
                    'pluralize' => false,
                    'extraPatterns' => ['GET sorteos' => 'loto/sorteos'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/loto',
                    'pluralize' => false,
                    'extraPatterns' => ['GET pozo-estimado' => 'loto/pozo-estimado'],
                    'tokens' => ['{sorteo}' => '<sorteo:\\w+>'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/loto',
                    'pluralize' => false,
                    'extraPatterns' => ['GET aciertos' => 'loto/aciertos'],
                    'tokens' => ['{sorteo}' => '<sorteo:\\w+>'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/loto',
                    'pluralize' => false,
                    'extraPatterns' => ['GET extracto' => 'loto/extracto'],
                    'tokens' => ['{sorteo}' => '<sorteo:\\w+>'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/quini-seis',
                    'pluralize' => false,
                    'extraPatterns' => ['GET sorteos' => 'quini-seis/sorteos'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/quini-seis',
                    'pluralize' => false,
                    'extraPatterns' => ['GET extracto' => 'quini-seis/extracto'],
                    'tokens' => ['{sorteo}' => '<sorteo:\\w+>'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/rebingo',
                    'pluralize' => false,
                    'extraPatterns' => ['GET sorteos' => 'rebingo/sorteos'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/rebingo',
                    'pluralize' => false,
                    'extraPatterns' => ['GET extracto' => 'rebingo/extracto'],
                    'tokens' => ['{sorteo}' => '<sorteo:\\w+>'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/telebingo-cordobes',
                    'pluralize' => false,
                    'extraPatterns' => ['GET sorteos' => 'telebingo-cordobes/sorteos'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/telebingo-cordobes',
                    'pluralize' => false,
                    'extraPatterns' => ['GET extracto' => 'telebingo-cordobes/extracto'],
                    'tokens' => ['{sorteo}' => '<sorteo:\\w+>'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/telebingo-cordobes',
                    'pluralize' => false,
                    'extraPatterns' => ['GET terminacion' => 'telebingo-cordobes/terminacion'],
                    'tokens' => ['{sorteo}' => '<sorteo:\\w+>'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/toto-bingo',
                    'pluralize' => false,
                    'extraPatterns' => ['GET sorteos' => 'toto-bingo/sorteos'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/toto-bingo',
                    'pluralize' => false,
                    'extraPatterns' => ['GET extracto' => 'toto-bingo/extracto'],
                    'tokens' => ['{sorteo}' => '<sorteo:\\w+>'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/quiniela',
                    'pluralize' => false,
                    'extraPatterns' => ['GET sorteos' => 'quiniela/sorteos'],
                    'tokens' => ['{nro}' => '<nro:\\w+>'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/quiniela',
                    'pluralize' => false,
                    'extraPatterns' => ['GET sorteo' => 'quiniela/sorteo'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/extracto',
                    'pluralize' => false,
                    'extraPatterns' => ['GET extracto' => 'quiniela/extracto'],
                    'tokens' => [['{sorteo}'=>'<sorteo:\\w+>'], ['{loteria}'=>'<loteria:\\w+>']],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/buscar',
                    'pluralize' => false,
                    'extraPatterns' => ['GET buscar' => 'quiniela/buscar'],
                    'tokens' => [['{sorteo}'=>'<sorteo:\\w+>'], ['{modalidad}'=>'<modalidad:\\w+>']],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/cabeza',
                    'pluralize' => false,
                    'extraPatterns' => ['GET cabeza' => 'quiniela/cabeza'],
                    'tokens' => [['{sorteo}'=>'<sorteo:\\w+>'], ['{loteria}'=>'<loteria:\\w+>']],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/loteria',
                    'pluralize' => false,
                    'extraPatterns' => ['GET sorteos' => 'loteria/sorteos'],
                    'tokens' => ['{sorteo}' => '<sorteo:\\w+>'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/extracto',
                    'pluralize' => false,
                    'extraPatterns' => ['GET extracto' => 'loteria/extracto'],
                    'tokens' => ['{sorteo}' => '<sorteo:\\w+>'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/rebote',
                    'pluralize' => false,
                    'extraPatterns' => ['GET rebote' => 'loteria/rebote'],
                    'tokens' => ['{sorteo}' => '<sorteo:\\w+>'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/significado',
                    'pluralize' => false,
                    'extraPatterns' => ['GET significado' => 'significado/suenios'],
                    'tokens' => ['{numero}' => '<numero:\\w+>'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/donde-esta-mi-billete',
                    'pluralize' => false,
                    'extraPatterns' => ['GET donde-esta-mi-billete' => 'donde-esta-mi-billete/buscar'],
                    'tokens' => [['{sorteo}'=>'<sorteo:\\w+>'], ['{billete}'=>'<billete:\\w+>']],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/donde-esta-mi-billete',
                    'pluralize' => false,
                    'extraPatterns' => ['GET donde-esta-mi-billete' => 'donde-esta-mi-billete/fracciones'],
                    'tokens' => [['{sorteo}'=>'<sorteo:\\w+>'], ['{billete}'=>'<billete:\\w+>']],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/tengo-premio',
                    'pluralize' => false,
                    'extraPatterns' => ['GET tengo-premio' => 'tengo-premio/sorteos-quiniela'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/tengo-premio',
                    'pluralize' => false,
                    'extraPatterns' => ['GET tengo-premio' => 'tengo-premio/buscar-quiniela'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/tengo-premio',
                    'pluralize' => false,
                    'extraPatterns' => ['GET tengo-premio' => 'tengo-premio/cierre-quiniela'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/tengo-premio',
                    'pluralize' => false,
                    'extraPatterns' => ['GET tengo-premio' => 'tengo-premio/sorteos-loteria'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/tengo-premio',
                    'pluralize' => false,
                    'extraPatterns' => ['GET tengo-premio' => 'tengo-premio/buscar-loteria'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/tengo-premio',
                    'pluralize' => false,
                    'extraPatterns' => ['GET tengo-premio' => 'tengo-premio/sorteos-toto-bingo'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/tengo-premio',
                    'pluralize' => false,
                    'extraPatterns' => ['GET tengo-premio' => 'tengo-premio/buscar-toto-bingo'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/tengo-premio',
                    'pluralize' => false,
                    'extraPatterns' => ['GET tengo-premio' => 'tengo-premio/sorteos-telebingo-cordobes'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/tengo-premio',
                    'pluralize' => false,
                    'extraPatterns' => ['GET tengo-premio' => 'tengo-premio/sorteos-rebingo'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/tengo-premio',
                    'pluralize' => false,
                    'extraPatterns' => ['GET tengo-premio' => 'tengo-premio/telebingo-cordobes-buscar'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/tengo-premio',
                    'pluralize' => false,
                    'extraPatterns' => ['GET tengo-premio' => 'tengo-premio/rebingo-buscar'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/localidades',
                    'pluralize' => false,
                    'extraPatterns' => ['GET localidades' => 'localidades/listar'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/barrios',
                    'pluralize' => false,
                    'extraPatterns' => ['GET barrios' => 'barrios/listar'],
                    'tokens' => ['{localidad}'=>'<localidad:\\w+>'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/calles',
                    'pluralize' => false,
                    'extraPatterns' => ['GET calles' => 'calles/listar'],
                    'tokens' => [['{localidad}'=>'<localidad:\\w+>'],['{barrio}'=>'<barrio:\\w+>']]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/nro-agencia',
                    'pluralize' => false,
                    'extraPatterns' => ['GET nro-agencia' => 'nro-agencia/listar'],
                    'tokens' => [['{localidad}'=>'<localidad:\\w+>'],['{barrio}'=>'<barrio:\\w+>'],['{calle}'=>'<calle:\\w+>']]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/datos-agencia',
                    'pluralize' => false,
                    'extraPatterns' => ['GET datos-agencia' => 'datos-agencia/listar'],
                    'tokens' => [['{localidad}'=>'<localidad:\\w+>'],['{nro_agencia}'=>'<nro_agencia:\\w+>']]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/agencia-cerrada',
                    'pluralize' => false,
                    'extraPatterns' => ['GET agencia-cerrada' => 'agencia-cerrada/listar'],
                    'tokens' => [['{localidad}'=>'<localidad:\\w+>'],['{nro_agencia}'=>'<nro_agencia:\\w+>']]
                ],
            ],
        ],
    ],
    'params' => $params,
];
