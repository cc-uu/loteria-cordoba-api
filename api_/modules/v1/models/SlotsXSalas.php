<?php 
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;

class SlotsXSalas extends ActiveRecord
{
    public static function tableName()
    {
        return 'SLOTS_X_SALAS';
    }
    public static function primaryKey()
    {
        return ['SXS_UID'];
    }
    public function rules()
    {
        return [
            [['SLO_UID', 'SAL_UID'], 'required']
        ];
    }
}