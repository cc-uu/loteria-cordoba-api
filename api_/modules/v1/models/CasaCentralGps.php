<?php 
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;

class CasaCentralGps extends ActiveRecord
{
    public static function tableName()
    {
        return 'CASACENTRAL_GPS';
    }
    public static function primaryKey()
    {
        return ['CEN_UID'];
    }
    public function rules()
    {
        return [
            [['CEN_UID', 'LATITUD', 'LONGITUD'], 'required']
        ];
    }
}