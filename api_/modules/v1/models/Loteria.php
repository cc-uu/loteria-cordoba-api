<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;

class Quiniela extends ActiveRecord
{
	public static function tableName()
	{
		return 'PARLOTE';
	}

    public static function primaryKey()
    {
        return ['SORTEO'];
    }
}