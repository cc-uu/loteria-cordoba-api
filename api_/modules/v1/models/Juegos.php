<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;

class Juegos extends ActiveRecord
{
	public static function tableName()
	{
		return 'JUEGOS';
	}

    public static function primaryKey()
    {
        return ['IDJUEGO'];
    }
}