<?php 
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;

class CasinosGps extends ActiveRecord
{
    public static function tableName()
    {
        return 'CASINO_GPS';
    }
    public static function primaryKey()
    {
        return ['CAS_UID'];
    }
    public function rules()
    {
        return [
            [['CAS_UID', 'LATITUD', 'LONGITUD'], 'required']
        ];
    }
}