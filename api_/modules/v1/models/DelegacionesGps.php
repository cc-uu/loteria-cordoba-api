<?php 
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;

class DelegacionesGps extends ActiveRecord
{
    public static function tableName()
    {
        return 'DELEGACION_GPS';
    }
    public static function primaryKey()
    {
        return ['DEL_UID'];
    }
    public function rules()
    {
        return [
            [['DEL_UID', 'LATITUD', 'LONGITUD'], 'required']
        ];
    }
}