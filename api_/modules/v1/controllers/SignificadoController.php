<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class SignificadoController extends ActiveController
{
	public $modelClass = 'api\modules\v1\models\Significado';

    public function behaviors(){
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionSuenios(){
		$connection = Yii::$app->db;	
		$request = Yii::$app->request;
		$numero = $request->get('numero');
		$xQuery="SELECT DESCRIPCION FROM SIGNIFICADO WHERE NUMERO=".$numero;
		$xSignificado=$connection->createCommand($xQuery)->queryAll();
		return $xSignificado;
	}
}