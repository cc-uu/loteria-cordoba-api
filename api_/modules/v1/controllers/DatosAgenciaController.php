<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class DatosAgenciaController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\DatosAgencia';

    public function behaviors() 
    {
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionListar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$localidad=$request->get('localidad');
		$barrio=$request->get('barrio');
		$calle=$request->get('calle');
		$nro_agen=$request->get('nro_agen');
		$age_uid=null;
		if ($localidad=="" && $barrio=="" && $calle=="" && $nro_agen==""){
			$xDatosAgencia=null;
		}
		else
		{
			if ($localidad!="" && $nro_agen!=""){
				$xQuery = "SELECT A.AGE_UID, A.NRO_AGEN, A.SUC_BAN, A.CALLE, A.NRO_CALLE, A.BARRIO, 
				A.LOCALIDAD, A.COD_POSTAL, A.TELEFONO, G.LATITUD, G.LONGITUD
				FROM JUEGOS.AGENCIA A
				LEFT JOIN AGENCIA_GPS G ON A.AGE_UID = G.AGE_UID
				WHERE A.NRO_AGEN IS NOT NULL 
				AND A.LOCALIDAD LIKE '%".$localidad."%' 
				AND A.NRO_AGEN=".$nro_agen."
				ORDER BY A.NRO_AGEN ASC";	
			}
			if ($localidad=="" && $nro_agen!=""){
				$xQuery = "SELECT A.AGE_UID, A.NRO_AGEN, A.SUC_BAN, A.CALLE, A.NRO_CALLE, A.BARRIO, 
				A.LOCALIDAD, A.COD_POSTAL, A.TELEFONO, G.LATITUD, G.LONGITUD
				FROM JUEGOS.AGENCIA A
				LEFT JOIN AGENCIA_GPS G ON A.AGE_UID = G.AGE_UID
				WHERE A.NRO_AGEN IS NOT NULL 
				AND A.NRO_AGEN=".$nro_agen."
				ORDER BY A.NRO_AGEN ASC";	
			}
			if ($localidad!="" && $nro_agen==""){
				$xQuery = "SELECT A.AGE_UID, A.NRO_AGEN, A.SUC_BAN, A.CALLE, A.NRO_CALLE, A.BARRIO, 
				A.LOCALIDAD, A.COD_POSTAL, A.TELEFONO, G.LATITUD, G.LONGITUD
				FROM JUEGOS.AGENCIA A
				LEFT JOIN AGENCIA_GPS G ON A.AGE_UID = G.AGE_UID
				WHERE A.NRO_AGEN IS NOT NULL 
				AND A.LOCALIDAD LIKE '%".$localidad."%' 
				ORDER BY A.NRO_AGEN ASC";	
			}
			if ($localidad!="" && $barrio!=""){
				$xQuery = "SELECT A.AGE_UID, A.NRO_AGEN, A.SUC_BAN, A.CALLE, A.NRO_CALLE, A.BARRIO, 
				A.LOCALIDAD, A.COD_POSTAL, A.TELEFONO, G.LATITUD, G.LONGITUD
				FROM JUEGOS.AGENCIA A
				LEFT JOIN AGENCIA_GPS G ON A.AGE_UID = G.AGE_UID
				WHERE A.NRO_AGEN IS NOT NULL 
				AND A.LOCALIDAD LIKE '%".$localidad."%' 
				AND A.BARRIO='".$barrio."' 
				ORDER BY A.NRO_AGEN ASC";	
			}
			if ($localidad!="" && $barrio!="" && $calle!=""){
				$xQuery = "SELECT A.AGE_UID, A.NRO_AGEN, A.SUC_BAN, A.CALLE, A.NRO_CALLE, A.BARRIO, 
				A.LOCALIDAD, A.COD_POSTAL, A.TELEFONO, G.LATITUD, G.LONGITUD
				FROM JUEGOS.AGENCIA A
				LEFT JOIN AGENCIA_GPS G ON A.AGE_UID = G.AGE_UID
				WHERE A.NRO_AGEN IS NOT NULL 
				AND A.LOCALIDAD LIKE '%".$localidad."%' 
				AND A.BARRIO='".$barrio."' 
				AND A.CALLE='".$calle."' 
				ORDER BY A.NRO_AGEN ASC";	
			}
			$xDatosAgencia=$connection->createCommand($xQuery)->queryAll();
		}
		$xParametros=array('datos_agencia'=>$xDatosAgencia);
		return $xParametros;
	}
}