<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class QuiniSeisController extends ActiveController
{
	public $modelClass = 'api\modules\v1\models\QuiniSeis';

    public function behaviors(){
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionSorteos(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$get_sorteo = $request->get('sorteo');
		$mes=date('m');
        $anio=date('Y');
        if ($mes=1 || $mes=2){
            $fecha="8/".($anio-1);
        }else{
            $fecha=(date('m')-2)."/".date('Y');
        }	
		$xQuery="SELECT TO_CHAR(FECHA,'dd/mm/yyyy') AS FECHA, SORTEO FROM PARQUI6 WHERE FECHA >= to_date('".$fecha."','mm/yyyy') ORDER BY SORTEO DESC";
		if($get_sorteo=="max"){
			$xQuery="SELECT TO_CHAR(FECHA,'dd/mm/yyyy') AS FECHA, SORTEO FROM PARQUI6 WHERE SORTEO IN (SELECT MAX(SORTEO) FROM PARQUI6)";
		}else if($get_sorteo!="" && $get_sorteo!="max"){
			$xQuery="SELECT TO_CHAR(FECHA,'dd/mm/yyyy') AS FECHA, SORTEO FROM PARQUI6 WHERE FECHA >= to_date('".$fecha."','mm/yyyy') AND SORTEO='".$get_sorteo."' GROUP BY FECHA,SORTEO ORDER BY SORTEO DESC";
		}
		$xSorteos=$connection->createCommand($xQuery)->queryAll();
		return $xSorteos;
	}

	public function actionExtracto(){
		$connection = Yii::$app->db;	
		$request = Yii::$app->request;
		$get_sorteo = $request->get('sorteo');
		$xQueryRondaUno="SELECT * FROM extractoqui6 WHERE sorteo='".$get_sorteo."' ORDER BY bolilla ";
		$xExtractoRondaUno=$connection->createCommand($xQueryRondaUno)->queryAll();
		return $xExtractoRondaUno;
	}
}