<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class AgenciaCerradaController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\AgenciaCerrada';

    public function behaviors() 
    {
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionListar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$nro_agen=$request->get('nro_agen');
		$suc_ban=$request->get('suc_ban');
		$fecha=date('d-m-y');
		$xQuery = "SELECT * FROM juegos.cierre WHERE nro_agen='".$nro_agen."' AND suc_ban='".$suc_ban."' AND FEC_DES >= to_date('".$fecha."','dd/mm/yy') AND FEC_HAS <= to_date('".$fecha."','dd/mm/yy') ORDER BY nro_agen";
		$xAgenciaCerrada=$connection->createCommand($xQuery)->queryAll();
		$xParametros=array('cierre'=>$xAgenciaCerrada);
		return $xParametros;
	}
}