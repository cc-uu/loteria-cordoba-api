<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class SlotsXSalasController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\SlotsXSalas';

    public function behaviors() 
    {
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}
	protected function verbs(){
	    return [
	        'create' => ['POST'],
	        'update' => ['POST'],
	    ];
	}
	public function actionListar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$slo_uid=$request->get('juego');
		$xQuery="SELECT SAL_UID, LATITUD, LONGITUD FROM SALA_GPS";
		if($slo_uid!=""){
			$xQuery="SELECT SALA_GPS.SAL_UID, SALA_GPS.LATITUD, SALA_GPS.LONGITUD FROM SLOTS_X_SALAS, SALA_GPS WHERE SLOTS_X_SALAS.SLO_UID=".$slo_uid." AND SALA_GPS.SAL_UID=SLOTS_X_SALAS.SAL_UID";
		}		
		$xCoordenadas=$connection->createCommand($xQuery)->queryAll();
		return $xCoordenadas;
	}
	public function actionCreate(){
		$params=$_REQUEST;
	    $model=new SlotsXSalas();
    	$model->attributes=$params;
 	    $model->save();
	}
	public function actionUpdate($id){
	    $params=$_REQUEST;
	    $model=$this->findModel($id);
	    $model->attributes=$params;
	    $model->save();
	}
}