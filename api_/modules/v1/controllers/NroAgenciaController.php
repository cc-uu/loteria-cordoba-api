<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class NroAgenciaController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\NroAgencia';

    public function behaviors() 
    {
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionListar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$localidad=$request->get('localidad');
		$barrio=$request->get('barrio');
		$calle=$request->get('calle');
		if($localidad=="" && $barrio==""){
			$xQuery = "SELECT NRO_AGEN 
			FROM juegos.agencia 
			WHERE NRO_AGEN IS NOT NULL 
			AND NRO_AGEN <= 3007 
			GROUP BY NRO_AGEN 
			ORDER BY NRO_AGEN";
		}
		if($localidad!="" && $barrio==""){
			$xQuery = "SELECT NRO_AGEN 
			FROM juegos.agencia 
			WHERE NRO_AGEN IS NOT NULL 
			AND NRO_AGEN <= 3007 
			AND LOCALIDAD LIKE '%".$localidad."%'
			GROUP BY NRO_AGEN 
			ORDER BY NRO_AGEN";
		}
		if($localidad!="" && $barrio!=""){
			$xQuery = "SELECT NRO_AGEN 
			FROM juegos.agencia 
			WHERE NRO_AGEN IS NOT NULL 
			AND NRO_AGEN <= 3007 
			AND BARRIO LIKE '%".$barrio."%' 
			AND LOCALIDAD LIKE '%".$localidad."%'
			GROUP BY NRO_AGEN 
			ORDER BY NRO_AGEN";
		}

		$xNroAgencia=$connection->createCommand($xQuery)->queryAll();
		$xParametros=array('nro_agencia'=>$xNroAgencia);
		return $xParametros;
	}
}