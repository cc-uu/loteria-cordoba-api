<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class CasinosGpsController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\CasinosGps';

    public function behaviors() 
    {
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}
	protected function verbs(){
	    return [
	        'create' => ['POST'],
	        'update' => ['POST'],
	    ];
	}
	public function actionListar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$cas_uid=$request->get('cas_uid');
		$xQuery="SELECT CAS_UID, LATITUD, LONGITUD FROM CASINO_GPS";
		if($cas_uid!=""){
			$xQuery="SELECT CAS_UID, LATITUD, LONGITUD FROM CASINO_GPS WHERE CAS_UID=".$cas_uid; 
		}		
		$xCoordenadas=$connection->createCommand($xQuery)->queryAll();
		return $xCoordenadas;
	}
	public function actionCreate(){
		$params=$_REQUEST;
	    $model=new CasinosGps();
    	$model->attributes=$params;
 	    $model->save();
	}
	public function actionUpdate($id){
	    $params=$_REQUEST;
	    $model=$this->findModel($id);
	    $model->attributes=$params;
	    $model->save();
	}
}