<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class LoteriaController extends ActiveController
{
	public $modelClass = 'api\modules\v1\models\Loteria';

    public function behaviors(){
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionSorteos(){
		$connection = Yii::$app->db;	
		$request = Yii::$app->request;
		$sorteo = $request->get('sorteo');
		$anterior = $request->get('anterior');
		$xQuery="SELECT MODALIDAD, SORTEO, TO_CHAR(FECHA, 'DD/MM/YYYY') AS FECHA, AGENCIA_GANADORA, PROGRESION FROM PARLOTE WHERE SORTEO IN(SELECT MAX(SORTEO) FROM PARLOTE)";

		if ($sorteo!=""){
			$xQuery="SELECT MODALIDAD, SORTEO, TO_CHAR(FECHA, 'DD/MM/YYYY') AS FECHA, AGENCIA_GANADORA, PROGRESION FROM PARLOTE WHERE SORTEO=".$sorteo;
			if($anterior=="si"){
				$xQuery="SELECT MODALIDAD, SORTEO, TO_CHAR(FECHA, 'DD/MM/YYYY') AS FECHA, AGENCIA_GANADORA, PROGRESION FROM PARLOTE WHERE SORTEO<".$sorteo." ORDER BY SORTEO DESC";
			}
		}

		$xSorteo=$connection->createCommand($xQuery)->queryAll();
		return $xSorteo;
	}

	public function actionExtracto(){
		$connection = Yii::$app->db;	
		$request = Yii::$app->request;
		$sorteo = $request->get('sorteo');
		$xQuery="SELECT POSICION, BOLILLA FROM EXTRACTOLOTE WHERE SORTEO=".$sorteo." ORDER BY POSICION ASC";
		$xSorteo=$connection->createCommand($xQuery)->queryAll();
		return $xSorteo;
	}

	public function actionRebote(){
		$connection = Yii::$app->db;	
		$request = Yii::$app->request;
		$sorteo = $request->get('sorteo');
		$xQuery="
		SELECT REBOTE.DIA, TO_CHAR(REBOTE.FECHA, 'DD/MM/YYYY') AS FECHAR, REBOTE.FECHA AS FECHAS, PARQUIN.SORTEO AS CONCURSO, 
		EXTRACTOQUIN.BOLILLA AS PREMIO
	    FROM REBOTE, PARQUIN, EXTRACTOQUIN 
	    WHERE REBOTE.SORTEO=".$sorteo."
	    AND PARQUIN.FECHA=REBOTE.FECHA
	    AND PARQUIN.MODALIDAD='NOCTURNO'
	    AND EXTRACTOQUIN.SORTEO=PARQUIN.SORTEO
	    AND EXTRACTOQUIN.LOTERIA=3
	    AND EXTRACTOQUIN.POSICION=1
	    ORDER BY REBOTE.FECHA";
		$xRebote=$connection->createCommand($xQuery)->queryAll();
		return $xRebote;
	}
}