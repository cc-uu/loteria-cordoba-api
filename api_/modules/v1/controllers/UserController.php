<?php
namespace api\modules\v1\controllers;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;

class UserController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\User';

    public function behaviors() 
    {
        return ArrayHelper::merge(parent::behaviors(), [
                'authenticator' => [
                    'class' => CompositeAuth::className(),
                    'authMethods' => [
                        QueryParamAuth::className(),
                    ]
                ],
        ]);
    }
}