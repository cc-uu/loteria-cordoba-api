<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class DondeEstaMiBilleteController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\DondeEstaMiBillete';

    public function behaviors() 
    {
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionSorteos(){
		$connection = Yii::$app->db;	
		$xQuery="SELECT SORTEO, TO_CHAR(FECHA_SORTEO, 'DD/MM/YYYY') AS FECHA FROM KANBAN.T_SORTEO WHERE ID_JUEGO=1 AND FECHA_BAJA IS NULL AND FECHA_SORTEO > SYSDATE ORDER BY SORTEO DESC";
		$xSorteo=$connection->createCommand($xQuery)->queryAll();
		return $xSorteo;
	}

	public function actionBuscar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$sorteo=$request->get('sorteo');
		$billete=$request->get('billete');
		$xQuery="SELECT COUNT(*) CANTIDAD, C.NOMBRE SUCURSAL, A.AGENCIA, A.DELEGACION, UPPER(B.NOMBRE) NOMBRE_AGENCIA, juegos.FC_RETORNAR_DOMICILIO_AGENCIA(A.SUC_BAN, B.NRO_AGEN) DOMICILIO,LPAD(B.NRO_AGEN, 5, 0) NRO_AGEN, VENTA_EMPLEADO, VENTA_CONTADO, DECODE(AGENCIA,'S','', DECODE(A.DELEGACION,'S', DECODE(NVL(B.NRO_AGEN, 0),0,'','ASIGNADA A LA AGENCIA Nro.' || LPAD(B.NRO_AGEN, 5, 0)),'')) DETALLE, CASE DECODE(A.AGENCIA, 'S', 1, 0) + DECODE(A.DELEGACION, 'S', 1, 0) + DECODE(A.ALMACEN_GENERAL, 'S', 1, 0) WHEN 1 THEN 'En Almacen' WHEN 2 THEN 'En Delegacion' WHEN 3 THEN 'En Agencia' END LUGAR, DECODE(A.AGENCIA, 'S', 1, 0) + DECODE(A.DELEGACION, 'S', 1, 0) + DECODE(A.ALMACEN_GENERAL, 'S', 1, 0) ORDEN FROM KANBAN.T_REPARTO_INTELIGENTE A, JUEGOS.AGENCIA B, JUEGOS.SUCURSAL C  WHERE A.SUC_BAN = B.SUC_BAN(+) AND A.NRO_AGEN = B.NRO_AGEN(+) AND A.SUC_BAN = C.SUC_BAN(+) AND A.ID_JUEGO = 1 AND A.SORTEO = ".$sorteo." AND A.SERIE = 1 AND A.BILLETE = ".$billete." GROUP BY A.ALMACEN_GENERAL, A.DELEGACION, A.AGENCIA, C.NOMBRE, B.NOMBRE, B.NRO_AGEN, A.SUC_BAN, VENTA_CONTADO, VENTA_EMPLEADO ORDER BY ORDEN";
		$xResultado=$connection->createCommand($xQuery)->queryAll();
		return $xResultado;
	}
}