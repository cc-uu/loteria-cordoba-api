<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class CallesController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Calles';

    public function behaviors() 
    {
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionListar(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$barrio=$request->get('barrio');
		$localidad=$request->get('localidad');
		if($barrio!="" && $localidad!=""){
			$xQuery = "SELECT CALLE 
			FROM juegos.agencia 
			WHERE CALLE IS NOT NULL 
			AND nro_agen <= 3007 
			AND BARRIO = '".$barrio."' 
			AND LOCALIDAD LIKE '%".$localidad."%'
			GROUP BY calle 
			ORDER BY calle";
		}		
		$xCalles=$connection->createCommand($xQuery)->queryAll();
		$xParametros=array('barrios'=>$xCalles);
		return $xParametros;
	}
}