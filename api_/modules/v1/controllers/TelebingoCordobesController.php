<?php
namespace api\modules\v1\controllers;
use \Yii;
use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class TelebingoCordobesController extends ActiveController
{
	public $modelClass = 'api\modules\v1\models\TelebingoCordobes';

    public function behaviors(){
	    $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
	}

	public function actionSorteos(){
		$connection = Yii::$app->db;
		$request = Yii::$app->request;
		$get_sorteo = $request->get('sorteo');
		$mes=date('m');
        $anio=date('Y');
        if ($mes=1 || $mes=2){
            $fecha="8/".($anio-1);
        }else{
            $fecha=(date('m')-2)."/".date('Y');
        }	
		$xQuery="SELECT TO_CHAR(FECHA,'dd/mm/yyyy')AS FECHA, SORTEO FROM extractotelebingo WHERE FECHA >= to_date('".$fecha."','mm/yyyy') GROUP BY FECHA,SORTEO ORDER BY SORTEO DESC";
		if($get_sorteo=="max"){
			$xQuery="SELECT TO_CHAR(FECHA,'dd/mm/yyyy')AS FECHA, SORTEO FROM extractotelebingo WHERE sorteo IN (SELECT MAX(sorteo) FROM extractotelebingo)";
		}else if($get_sorteo!="" && $get_sorteo!="max"){
			$xQuery="SELECT TO_CHAR(FECHA,'dd/mm/yyyy')AS FECHA, SORTEO FROM extractotelebingo WHERE FECHA >= to_date('".$fecha."','mm/yyyy') AND SORTEO='".$get_sorteo."' GROUP BY FECHA,SORTEO ORDER BY SORTEO DESC";
		}
		$xSorteos=$connection->createCommand($xQuery)->queryAll();
		return $xSorteos;
	}

	public function actionExtracto(){
		$connection = Yii::$app->db;	
		$request = Yii::$app->request;
		$get_sorteo = $request->get('sorteo');
		$xQueryRondaUno="SELECT * FROM extractotelebingo WHERE ronda='1' AND sorteo='".$get_sorteo ."' ORDER BY registro";
		$xQueryRondaDos="SELECT * FROM extractotelebingo WHERE ronda='2' AND sorteo='".$get_sorteo ."' ORDER BY registro";
		$xQueryRondaTre="SELECT * FROM extractotelebingo WHERE ronda='3' AND sorteo='".$get_sorteo ."' ORDER BY registro";
		$xExtractoRondaUno=$connection->createCommand($xQueryRondaUno)->queryAll();
		$xExtractoRondaDos=$connection->createCommand($xQueryRondaDos)->queryAll();
		$xExtractoRondaTre=$connection->createCommand($xQueryRondaTre)->queryAll();
		$xParametros=array(
			'ronda_uno'=>$xExtractoRondaUno,
			'ronda_dos'=>$xExtractoRondaDos,
			'ronda_tre'=>$xExtractoRondaTre);
		return $xParametros;
	}

	public function actionTerminacion(){
		$connection = Yii::$app->db;	
		$request = Yii::$app->request;
		$get_sorteo = $request->get('sorteo');
		$xQuery="SELECT * FROM extractotelebingo WHERE ronda=0 AND sorteo='".$get_sorteo ."' ORDER BY registro";
		$xTerminacion=$connection->createCommand($xQuery)->queryAll();
		return $xTerminacion;
	}
}